---
layout: page
---

# About

Throughout the history, technologies has always helped human race to progress forward. But these technologies has become a tool of oppression for the mighty people. SOTC is a community formed by the students of Pondicherry University to talk and react to these issues, with a moto of making technology open, free and inclusive. This community strives to bring people from every starta of the society and all the quarters of the country, to generate more inclusive and dynamic ideas. A space like PU campus is a boon for this community to bring together young minds from various academic discipline as well as from different parts of the country. Ever since the community was formed it has organised several talks, discussions and different events on areas like knowledge commons includes open access to research journals, data privacy & surveillance, and Free & Open Source Software. The community needs you to work towards the new challenges in technological landscape, narrowing down the prevailing divide, and to make technology more people friendly. Join SOTC signal group to know more about the community and to share your views and discuss.